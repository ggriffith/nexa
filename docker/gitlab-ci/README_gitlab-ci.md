### docker-ci-nexa

Docker files to create an image to use in GitLab CI

The data in this folder has been deprecated, please use the files and tools you can find at this git repo:

https://gitlab.com/nexa/docker-ci-nexa
