```
getminingmaxblock

Return the max generated (mined) block size
Result
      (integer) maximum generated block size in bytes

Examples:
> nexa-cli getminingmaxblock 
> curl --user myusername --data-binary '{"jsonrpc": "1.0", "id":"curltest", "method": "getminingmaxblock", "params": [] }' -H 'content-type: text/plain;' http://127.0.0.1:7227/

```
